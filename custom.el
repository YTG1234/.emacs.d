(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auth-source-save-behavior nil)
 '(display-line-numbers-type 'visual)
 '(emms-lyrics-display-on-minibuffer t)
 '(emms-lyrics-display-on-modeline t)
 '(hyperbole-web-search-alist
   '(("Amazon" . "http://www.amazon.com/s/field-keywords=%s")
	 ("Bing" . "http://www.bing.com/search?q=%s")
	 ("Dictionary" . "https://en.wiktionary.org/wiki/%s")
	 ("Elisp" . "http://www.google.com/search?q=%s+filetype:el")
	 ("Facebook" . "https://www.facebook.com/hashtag/%s")
	 ("Google" . "http://www.google.com/search?q=%s")
	 ("gitHub" . "https://github.com/search?ref=simplesearch&q=%s")
	 ("Images" . "http://www.google.com/images?hl=en&q=%s")
	 ("Jump" . webjump)
	 ("Maps" . "http://maps.google.com/maps?q=%s")
	 ("RFCs" . "https://tools.ietf.org/html/rfc%s")
	 ("StackOverflow" . "https://stackoverflow.com/search?q=%s")
	 ("Twitter" . "https://twitter.com/search?q=%s")
	 ("Wikipedia" . "https://en.wikipedia.org/wiki/%s")
	 ("Youtube" . "https://www.youtube.com/results?search_query=%s")
	 ("Brave Search" . "https://search.brave.com/search?q=%s")))
 '(lsp-haskell-completion-in-comments nil)
 '(lsp-haskell-max-completions 100)
 '(lsp-haskell-plugin-hlint-code-actions-on t)
 '(lsp-haskell-plugin-hlint-config-flags [])
 '(lsp-haskell-plugin-pragmas-code-actions-on t)
 '(lsp-haskell-plugin-tactics-config-auto-gas 20)
 '(lsp-haskell-plugin-tactics-config-hole-severity 2)
 '(lsp-haskell-plugin-tactics-config-timeout-duration 30)
 '(lsp-haskell-plugin-tactics-global-on t)
 '(lsp-haskell-server-wrapper-function 'identity)
 '(org-auto-tangle-babel-safelist (list (expand-file-name "Config.org" user-emacs-directory)))
 '(org-directory
   (expand-file-name "Documents/Org"
					 (or
					  (getenv "NXC_ROOT")
					  (expand-file-name "~/Nextcloud"))))
 '(repeat-mode t)
 '(sentence-end-double-space nil)
 '(undo-ask-before-discard t)
 '(vr/default-replace-preview t)
 '(ytg/font-size-mono 125)
 '(ytg/font-size-var 125)
 '(ytg/indent-level 4)
 '(ytg/js-indent-level 2)
 '(ytg/languagetool-cmdline-path "~/.nix-profile/share/languagetool-commandline.jar")
 '(ytg/mbsync-use-xdg t)
 '(ytg/mono-fonts '("Fira Code" "FiraCode Nerd Font"))
 '(ytg/mu4e-use-xdg t)
 '(ytg/roam-use-home nil)
 '(ytg/use-doom-modeline t)
 '(ytg/var-fonts '("Noto Sans")))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(whitespace-big-indent ((t (:background "#552222" :foreground "firebrick")))))
