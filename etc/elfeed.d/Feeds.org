# Align tags: C-u C-c C-q

* Feeds                                                              :elfeed:
** [[https://feeds.feedburner.com/HaveIBeenPwnedLatestBreaches][Have I been Pwned?]]                                       :pwned:security:
** [[https://odysee.com/$/rss/@NaomiBrockwell:4][Naomi Brockwell on Odysee]]                    :security:privacy:brockwell:
** Linux                                                             :linux:
*** [[https://archlinux.org/feeds/news/][Arch Linux]]                                                  :archlinux:
*** [[https://www.linuxfoundation.org/feed/][Linux Foundation]]                                          :lfoundation:
*** [[https://voidlinux.org/atom.xml][Void Linux]]                                                  :voidlinux:
*** [[https://lwn.net/headlines/newrss][LWN.net]]                                                           :lwn:
** Programming                                                 :programming:
*** [[http://book.realworldhaskell.org/feeds/comments/][Real World Haskell]]                                               :rwhs:
** Music                                                             :music:
*** [[https://www.mutopiaproject.org/latestadditions.rss][Mutopia]]
** News
*** [[https://www.haaretz.co.il/srv/rss---feedly][Haaretz News]]                                             :haaretz:news:
*** [[https://www.haaretz.co.il/srv/htz---culture---rss][Haaretz Culture]]                                               :haaretz:
*** [[https://www.haaretz.co.il/srv/rss-opinion][Haaretz Opinion]]                                               :haaretz:
