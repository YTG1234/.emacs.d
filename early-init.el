(setcar native-comp-eln-load-path
              (expand-file-name "emacs/eln-cache/"
                                                (or (getenv "XDG_CACHE_HOME")
                                                        "~/.cache")))
