(add-hook 'emacs-startup-hook (lambda ()
                (message "Emacs loaded in %s with %d garbage collections."
                         (format "%.2f seconds"
                         (float-time
                          (time-subtract after-init-time before-init-time)))
                     gcs-done)))

(setq %ytg/graphics (or (display-graphic-p) (daemonp)))
(setq %ytg/home-manager (and (locate-library "ytg-nix-home-manager")))

(defun pathor (paths)
  (if paths
      (if (file-exists-p (car paths))
          (car paths)
        (pathor (cdr paths)))
    nil))

(setq ytg/emacs-data (expand-file-name "emacs/"
                                       (or (getenv "XDG_DATA_HOME")
                                           "~/.local/share/" ))
      ytg/emacs-state (expand-file-name "emacs/"
                                        (or (getenv "XDG_STATE_HOME")
                                            "~/.local/state/")))

(defgroup personal nil
  "Values for YTG's personal configuration."
  :group 'emacs)

(defcustom ytg/mono-fonts '("Fira Code" "FiraCode Nerd Font")
  "Available monospace fonts."
  :group 'personal
  :type '(repeat string))

(defcustom ytg/var-fonts '("Noto Sans")
  "Available variable-pitch fonts."
  :group 'personal
  :type '(repeat string))

(defcustom ytg/font-size-mono 125
  "Font sized used for fixed-pitch text."
  :group 'personal
  :type 'natnum)

(defcustom ytg/font-size-var 125
  "Font sized used for variable-pitch text."
  :group 'personal
  :type 'natnum)

(defcustom ytg/indent-level 4
  "General indent level for code."
  :group 'personal
  :type 'natnum)

(defcustom ytg/js-indent-level 4
  "Indent level for Javascript."
  :group 'personal
  :type 'natnum)

(defcustom ytg/mbsync-use-xdg t
  "Whether mbsync should conform to XDG conventions."
  :group 'personal
  :type 'boolean)

(defcustom ytg/mu4e-use-xdg t
  "Whether mu4e should conform to XDG conventions."
  :group 'personal
  :type 'boolean)

(defcustom ytg/roam-use-home nil
  "Whether Org Roam should dwell in `~/Documents/OrgRoam'."
  :group 'personal
  :type 'boolean)

(defcustom ytg/use-doom-modeline t
  "Whether Doom modeline should be enabled."
  :group 'personal
  :type 'boolean)

(defcustom ytg/languagetool-cmdline-path "/usr/share/languagetool-commandline.jar"
  "The path to languagetool-commandline.jar."
  :group 'personal
  :type 'file)

(defcustom ytg/mpd-options '("crossfade 5" "consume 0")
  "Commands to send to MusicPD upon connection."
  :group 'personal
  :type '(repeat string))

(setq org-auto-tangle-babel-safelist
      (list (expand-file-name "Config.org" user-emacs-directory)))
(setq custom-file (expand-file-name "custom.el" ytg/emacs-data))
(load (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))

(add-to-list 'load-path (expand-file-name "site-lisp/" user-emacs-directory))

;; Nix (isn't there when Emacs is installed outside of Nix)
(when (file-directory-p "~/.nix-profile/share/emacs")
  (add-to-list 'load-path "~/.nix-profile/share/emacs/site-lisp"))
(when (file-directory-p
         (expand-file-name
              "nix/profile/share/emacs"
              (or (getenv "XDG_STATE_HOME")
                      "~/.local/state/")))
  (add-to-list
   'load-path
   (expand-file-name
      "nix/profile/share/emacs"
      (or (getenv "XDG_STATE_HOME")
              "~/.local/state/"))))

;; Emacs doesn't seem to do that by itself
(dolist (loaddir load-path)
  (let ((sitestart (expand-file-name "site-start.d/" loaddir)))
    (when (file-directory-p sitestart)
      (dolist (file (directory-files-recursively sitestart "\\.el"))
        (load-file file)))))

(setq straight-base-dir ytg/emacs-data)
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" straight-base-dir))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)
(setq straight-use-package-by-default (not %ytg/home-manager))

;; Define directories to save files in
(let ((ytg/backup-directory (expand-file-name "backups/" ytg/emacs-data))
      (ytg/autosave-directory (expand-file-name "auto-saves/" ytg/emacs-state)))

  ;; Set the actual variables
  (setq
   backup-directory-alist `(("." . ,ytg/backup-directory)
                            (,tramp-file-name-regexp . nil))
   tramp-backup-directory-alist '(("." . nil))
   auto-save-list-file-prefix (expand-file-name "sessions/" ytg/autosave-directory)
   auto-save-file-name-transforms `((".*" ,ytg/autosave-directory) t)))

(use-package no-littering
  :init
  (setq no-littering-var-directory ytg/emacs-data)
  (require 'no-littering))

(setq backup-by-copying-when-linked t)
(setq delete-old-versions t
      kept-new-versions 4
      kept-old-versions 2
      version-control t)

(use-package evil-numbers
  :commands (evil-numbers/inc-at-pt evil-numbers/dec-at-pt))

(use-package hydra
  :defer t
  :preface
  (defun ytg/tab-move-right () (interactive) (tab-move 1))
  (defun ytg/tab-move-left () (interactive) (tab-move -1))

  :config (defhydra hydra-tab-move (:timeout 4)
            "Move Tabs"
            ("l" ytg/tab-move-right "Right")
            ("h" ytg/tab-move-left "Left")
            ("q" nil "Finish" :exit t)))

(use-package general
  :after hydra
  ;; :init (general-evil-setup)
  :config
  (windmove-default-keybindings)
  (general-create-definer ytg/leader-keys
    ;; :keymaps '(normal insert visual emacs)
    :prefix "C-c")

  (general-create-definer ytg/global-key)
    ;; :keymaps '(normal insert visual emacs)

  (ytg/leader-keys
    "t" '(:ignore t :which-key "toggle")
    "o" '(:ignore t :which-key "open")
    "w" '(:ignore t :which-key "windows")
    "v" '(:ignore t :which-key "tabs")
    "e" '(:ignore t :which-key "evaluate")
    "d" '(:ignore t :which-key "describe")

    "tt" '(counsel-load-theme :which-key "Choose Theme")
    "tp" '(treemacs :which-key "Toggle Treemacs")

    "os" '(eshell :which-key "Open Eshell in Same Window")
    "og" '(magit-status :which-key "Open Magit")
    "om" '(mu4e :which-key "Open mu4e")
    "ob" '(scratch :which-key "New scratch buffer")

    ;; "wh" '(evil-window-left :which-key "Switch One Window Left")
    ;; "wj" '(evil-window-down :which-key "Switch One Window Down")
    ;; "wk" '(evil-window-up :which-key "Switch One Window Up")
    ;; "wl" '(evil-window-right :which-key "Switch One Window Right")
    ;; "ws" '(evil-window-split :which-key "Create a Horizontal Split")
    ;; "wv" '(evil-window-vsplit :which-key "Create a Vertical Split")
    ;; "wc" '(evil-window-delete :which-key "Delete Current Window")
    "wb" '(switch-to-minibuffer :which-key "Switch to Minibuffer")

    "vt" '(tab-new :which-key "New Tab")
    "vr" '(tab-rename :which-key "Rename Tab")
    "vw" '(tab-close :which-key "Close Tab")
    "vW" '(tab-close-other :which-key "Close Other Tabs")
    "vT" '(tab-bar-undo-close-tab :which-key "Reopen Closed Tabs")
    "vm" '(hydra-tab-move/body :which-key "Move Tab")
    "vf" '(tab-bar-select-tab-by-name :which-key "Find Tab")

    "ex" '(eval-last-sexp :which-key "Evaluate Last S-expression")
    "er" '(eval-region :which-key "Evaluate Selected Region")
    "eb" '(eval-buffer :which-key "Evaluate Current Buffer")

    "df" '(describe-function :which-key "Describe Function")
    "dv" '(describe-variable :which-key "Describe Variable")
    "dk" '(describe-key :which-key "Describe Key")
    "dm" '(describe-mode :which-key "Describe Mode")
    "dF" '(counsel-describe-face :which-key "Describe Face"))

  (general-define-key ;; ytg/global-key
    "<escape>" 'keyboard-escape-quit

    "C-=" 'text-scale-increase
    "C--" 'text-scale-decrease

    "s-k" 'scroll-down-line
    "s-j" 'scroll-up-line

    "C-c +" 'evil-numbers/inc-at-pt
    "C-c -" 'evil-numbers/dec-at-pt
    "C-c x +" 'ytg/increment-number-hexadecimal
    "C-c x -" 'ytg/decrement-number-hexadecimal
    "C-c b +" 'ytg/increment-number-binary
    "C-c b -" 'ytg/decrement-number-binary)

(defvar-keymap evil-numbers-repeat-map
  :repeat t
  "+" #'evil-numbers/inc-at-pt
  "-" #'evil-numbers/dec-at-pt)
(defvar-keymap ytg/hexadecimal-repeat-map
  :repeat t
  "+" #'ytg/increment-number-hexadecimal
  "-" #'ytg/decrement-number-hexadecimal)
(defvar-keymap ytg/binary-repeat-map
  :repeat t
  "+" #'ytg/increment-number-binary
  "-" #'ytg/decrement-number-binary)

(put 'evil-numbers/inc-at-pt 'repeat-map 'evil-numbers-repeat-map)
(put 'evil-numbers/dec-at-pt 'repeat-map 'evil-numbers-repeat-map)
(put 'ytg/increment-number-hexadecimal 'repeat-map 'ytg/hexadecimal-repeat-map)
(put 'ytg/decrement-number-hexadecimal 'repeat-map 'ytg/hexadecimal-repeat-map)
(put 'ytg/increment-number-binary 'repeat-map 'ytg/binary-repeat-map)
(put 'ytg/decrement-number-binary 'repeat-map 'ytg/binary-repeat-map))

(defun %ytg/diminish-setup () (interactive)
  (dolist (mode '(auto-revert-mode
                  editorconfig-mode
                  buffer-face-mode
                  eldoc-mode
                  visual-line-mode))
    (diminish mode)))
(use-package diminish
  :demand
  :init (%ytg/diminish-setup)
  :config (%ytg/diminish-setup)
  :hook (after-change-major-mode . %ytg/diminish-setup))

(setq inhibit-startup-message t) ; Disable startup screen

(when %ytg/graphics
  (when (eq system-type 'darwin) (scroll-bar-mode -1)) ; Disable Scrollbar
  (set-fringe-mode 10))
(tool-bar-mode -1) ; Disable tool bar
(tooltip-mode -1) ; Display tooltips
(menu-bar-mode -1) ; Disable menu bar

(column-number-mode)
(global-display-line-numbers-mode t)

(dolist (mode '(org-mode-hook
                markdown-mode-hook
                term-mode-hook
                vterm-mode-hook
                shell-mode-hook
                treemacs-mode-hook
                eshell-mode-hook
                pdf-view-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(defun ytg/try-fonts (fonts)
  (when fonts
    (if (x-list-fonts (car fonts))
        (car fonts)
      (ytg/try-fonts (cdr fonts)))))

;; Defining a function that sets the font settings
(defun ytg/set-fonts ()
  (set-fontset-font "fontset-default" 'hebrew (font-spec :family "Liberation Mono"))
  (set-fontset-font "fontset-default" 'arabic (font-spec :family "Noto Sans Arabic"))
  (let ((mono (ytg/try-fonts ytg/mono-fonts))
        (var (ytg/try-fonts ytg/var-fonts)))
      (custom-set-faces
       `(default ((t (:height ,ytg/font-size-mono :family ,mono :weight regular))))
       `(fixed-pitch ((t (:height ,ytg/font-size-mono :family ,mono :weight regular))))
       `(tab-bar ((t (:height ,ytg/font-size-var :family ,var :weight regular))))
       `(variable-pitch ((t (:weight regular :height ,ytg/font-size-var :family ,var)))))))

;; Set the fonts when opening the frame if running in daemon mode, otherwise just set the fonts
(if (daemonp)
    (progn (add-hook 'after-make-frame-functions
                     (lambda (frame)
                       (setq doom-modeline-icon t)
                       (with-selected-frame frame (ytg/set-fonts)))))
  (when (display-graphic-p)
    (ytg/set-fonts)))

;; Don't care about theming when running in a TTY
(when %ytg/graphics
  (use-package doom-themes :defer t)
  (load-theme 'doom-dark+ t)
  (doom-themes-visual-bell-config))

(unless %ytg/graphics
  ;; Setup the Xclip package
  (use-package xclip
    :commands xclip-mode
    :hook (tty-setup . (lambda () (with-demoted-errors "%s" (xclip-mode +1))))
    :diminish xclip-mode)

  (setq xterm-set-window-tite t) ; Requires Emacs 27+
  (setq visible-cursor nil)

  ;; (use-package evil-terminal-cursor-changer
  ;;   :commands evil-terminal-cursor-changer-activate
  ;;   :hook (tty-setup . evil-terminal-cursor-changer-activate)
  ;;   :straight (:host github
  ;;                    :repo "kisaragi-hiu/evil-terminal-cursor-changer"))

  (use-package xterm-mouse-mode
    :straight nil
    :commands xterm-mouse-mode
    :hook (after-init . xterm-mouse-mode)))

(use-package ivy
  :diminish ivy-mode
  :general
  (:keymaps 'ivy-minibuffer-map
            "TAB" 'ivy-alt-done
            "C-l" 'ivy-alt-done
            "C-j" 'ivy-next-line
            "C-k" 'ivy-previous-line)
  (:keymaps 'ivy-switch-buffer-map
            "C-k" 'ivy-previous-line
            "C-l" 'ivy-done
            "C-d" 'ivy-switch-buffer-kill)
  (:keymaps 'ivy-reverse-i-search-map
            "C-k" 'ivy-previous-line
            "C-d" 'ivy-reverse-i-search-kill)
  :hook (after-init . ivy-mode))

(use-package counsel
  :diminish counsel-mode
  :after ivy
  :general
  (:keymaps '(counsel-mode-map minibuffer-local-map)
            "C-s" 'counsel-grep-or-swiper
            "C-r" 'counsel-grep-or-swiper-backward)
  :init (counsel-mode 1))

(use-package all-the-icons-ivy-rich
  :after ivy
  :diminish all-the-icons-ivy-rich-mode)

(use-package ivy-rich
  :after (ivy all-the-icons-ivy-rich)
  :diminish ivy-rich-mode
  :hook (ivy-mode . (lambda () (all-the-icons-ivy-rich-mode 1) (ivy-rich-mode 1))))

(use-package all-the-icons)
(use-package all-the-icons-dired
  :after (all-the-icons dired)
  :commands all-the-icons-dired-mode)

(use-package dired
  :straight nil
  ;; :after evil-collection
  :config
  (setq dired-listing-switches "-agho --group-directories-first"
        dired-omit-files "^\\.[^.].*"
        dired-omit-verbose nil
        dired-hide-details-hide-symlink-targets nil
        delete-by-moving-to-trash t)

  (autoload 'dired-omit-mode "dired-x")
  (add-hook 'dired-load-hook
            (lambda () (interactive)
              (dired-collapse)))
  (add-hook 'dired-mode-hook
            (lambda () (interactive)
              (dired-omit-mode 1)
              (dired-hide-details-mode 1)
              (all-the-icons-dired-mode 1)
              (hl-line-mode 1)))

  (use-package dired-collapse :defer t)
  (use-package dired-single :defer t)
  (use-package dired-ranger :defer t))

;; (evil-collection-define-key 'normal 'dired-mode-map
;;   "h" 'dired-single-up-directory
;;   "H" 'dired-omit-mode
;;   "l" 'dired-single-buffer
;;   "y" 'dired-ranger-copy
;;   "x" 'dired-ranger-move
;;   "p" 'dired-ranger-paste))

(use-package ls-lisp
  :if (eq system-type 'darwin)
  :straight nil
  :after dired
  :config (require 'ls-lisp)
  :custom ((ls-lisp-use-insert-directory-program nil)))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.3))

(use-package mwheel
  :if %ytg/graphics
  :defer t
  :straight nil
  :custom ((mouse-wheel-scroll-amount '(1
                                        ((shift) . 5)
                                        ((control))))
           (mouse-wheel-progressive-speed nil)))

(use-package pixel-scroll
  :if %ytg/graphics
  :commands (pixel-scroll-mode)
  :straight nil
  :hook (after-init . pixel-scroll-mode))

(use-package warnings
  :straight nil
  :custom (warning-suppress-types '((comp))))

(use-package whitespace
  :if %ytg/graphics
  :straight nil
  :hook (prog-mode . whitespace-mode)
  :config
  (delete 'lines whitespace-style)
  (delete 'newline whitespace-style)
  (delete 'newline-mark whitespace-style))

(setq-default indent-tabs-mode t)
(setq-default tab-width ytg/indent-level)
(defvaralias 'c-basic-offset 'ytg/indent-level)
(defvaralias 'cperl-indent-level 'ytg/indent-level)

(use-package editorconfig
  :commands (editorconfig-mode)
  :diminish editorconfig-mode
  :hook (after-init . editorconfig-mode))

(use-package rainbow-delimiters
  :diminish rainbow-delimiters-mode
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package helpful
  :commands (helpful-callable helpful-variable helpful-command helpful-key)
  :custom ((counsel-describe-function-function #'helpful-callable)
           (counsel-describe-variable-function #'helpful-variable))
  :general
  ([remap describe-function] 'counsel-describe-function
   [remap describe-command] 'helpful-command
   [remap describe-variable] 'counsel-describe-variable
   [remap describe-key] 'helpful-key))

(use-package evil-nerd-commenter
  :general ("M-/" 'evilnc-comment-or-uncomment-lines))

(use-package hideshow
  :straight nil
  :hook (prog-mode . hs-minor-mode))

(use-package multiple-cursors
  :defer t
  :config
  (general-define-key ;; ytg/global-key
    "C-S-c C-S-c" 'mc/edit-lines
    "C->" 'mc/mark-next-like-this
    "C-<" 'mc/mark-previous-like-this
    "C-c C-<" 'mc/mark-all-like-this))

(use-package visual-regexp
  :after multiple-cursors
  :defer t
  :commands (vr/replace vr/query-replace vr/mc-mark)
  :config
  (ytg/leader-keys
    "r" '(vr/replace :which-key "Replace regexp non-interactively")
    "q" '(vr/query-replace :which-key "Query-replace regexp")
    "m" '(vr/mc-mark :which-key "Multiple cursors at regexp selection")))

(use-package avy
  :custom (avy-style 'pre)
  :general
  (;; ytg/global-key
    "C-c C-j" '(avy-resume :which-key "Continue Avy action")
    "C-;" '(avy-goto-char :which-key "Goto Char")
    "C-'" '(avy-goto-char-2 :which-key "Goto two Chars")
    "M-g j" '(avy-goto-char-timer :which-key "Goto Chars")
    "M-g f" '(avy-goto-line :which-key "Goto Line")
    "M-g w" '(avy-goto-word-1 :which-key "Goto Word (1)")
    "C-j" '(avy-goto-word-1 :which-key "Goto Word (1)")
    "M-g e" '(avy-goto-word-0 :which-key "Goto Word")))

(use-package magit
  :commands (magit-status magit-get-current-branch)
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(use-package projectile
  :diminish projectile-mode
  :defer t
  :config (projectile-mode 1)
  :general ("C-c p" '(:keymap projectile-command-map :which-key "Projectile"))
  :custom ((projectile-project-search-path '("~"))
           (projectile-switch-project-action #'projectile-dired)
           (projectile-enable-caching t)))

(use-package counsel-projectile
  :diminish counsel-projectile-mode
  :after (projectile counsel)
  :config (counsel-projectile-mode 1))

(defun ytg/org-setup ()
  (org-indent-mode 1)
  (variable-pitch-mode 1) ; non-monospace font
  (visual-line-mode 1)
  (diminish 'org-indent-mode)
  ;; (diminish 'org-auto-tangle-mode "oat")
  )

(defun ytg/org-font-setup ()
  ;; Bulleted Lists
  (font-lock-add-keywords 'org-mode '(("^ *\\([-]\\) "
                                       (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  ;; Title sizes
  (dolist (face '((org-level-1 . 1.2)
                  (org-level-2 . 1.1)
                  (org-level-3 . 1.05)
                  (org-level-4 . 1.0)
                  (org-level-5 . 1.1)
                  (org-level-6 . 1.1)
                  (org-level-7 . 1.1)
                  (org-level-8 . 1.1)))
    (set-face-attribute (car face) nil :weight 'regular :height (cdr face)))

  ;; Monospace for things that need it
  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-table nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-formula nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)
  (set-face-attribute 'line-number nil :inherit 'fixed-pitch)
  (set-face-attribute 'line-number-current-line nil :inherit 'fixed-pitch))

(use-package org
  :commands (org-capture org-agenda)
  :hook (org-mode . ytg/org-setup)
  :config
  (when %ytg/graphics (ytg/org-font-setup))

  :custom ((org-confirm-babel-evaluate nil)
           (org-startup-with-inline-images t)
           (org-image-actual-width nil)))

(use-package org-contrib
  :after org)

(use-package org-bullets
  :defer t
  :diminish org-bullets-mode
  :hook (org-mode . org-bullets-mode)
  :custom (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(use-package org
  :straight nil
  :defer t
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (python . t)
     (clojure . t)
     (java . t)
     (lisp . t)
     (shell . t)
     (C . t)))

(require 'org-tempo)
(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("scm" . "src scheme"))
(add-to-list 'org-structure-template-alist '("py" . "src python"))
(add-to-list 'org-structure-template-alist '("java" . "src java"))
(add-to-list 'org-structure-template-alist '("src" . "src"))

:custom (org-todo-keywords '((type "TODO" "|" "DONE" "CANCELED"))))

(use-package org-auto-tangle
  :after org :defer t
  :hook (org-mode . org-auto-tangle-mode)
  :diminish (org-auto-tangle-mode . "oat"))

(use-package htmlize
  :after org)

(use-package ox-reveal
  :after (org htmlize)
  :custom ((org-reveal-root (expand-file-name "reveal.js" user-emacs-directory))
           (org-reveal-hlevel 2)))

(use-package org
  :defer t
  :straight nil
  :config
  (add-to-list 'org-latex-packages-alist
               '("AUTO" "babel" t ("pdflatex")))
  (add-to-list 'org-latex-packages-alist
               '("AUTO" "polyglossia" t ("xelatex" "lualatex")))
  :custom ((org-latex-compiler "xelatex")
           (org-latex-pdf-process
            '("latexmk -f -pdfxe -%latex -interaction=nonstopmode -output-directory=%o %f"))))

(use-package org
  :defer t
  :custom (org-hide-emphasis-markers t))
(use-package org-appear
  :hook (org-mode . org-appear-mode))

(use-package org-present
      :after (org visual-fill-column)
      :commands (org-present)
      :config
      (add-hook
       'org-present-mode-hook
       (lambda ()
         (setq-local face-remapping-alist
                                 '((default (:height 1.5) variable-pitch)
                                       (header-line (:height 4.0) variable-pitch)
                                       (org-document-title (:height 1.75) org-document-title)
                                       (org-code (:height 1.55) org-code)
                                       (org-verbatim (:height 1.55) org-verbatim)
                                       (org-block (:height 1.25) org-block)
                                       (org-block-begin-line (:height 0.7) org-block)))
         (setq header-line-format " ")
         (org-display-inline-images)
         (visual-fill-column-mode 1)
         (visual-line-mode 1)
         (org-appear-mode -1)))
      (add-hook
       'org-present-mode-quit-hook
       (lambda ()
         (setq-local face-remapping-alist '((default variable-pitch default)))
         (setq header-line-format nil)
         (org-remove-inline-images)
         (visual-fill-column-mode 0)
         (visual-line-mode 0)
         (org-appear-mode 1)))
      (add-hook
       'org-present-after-navigate-functions
       (lambda (buf-name heading)
         (org-overview)
         (org-show-entry)
         (org-show-children))))

(use-package visual-fill-column
      :defer t
      :commands (visual-fill-column-mode)
      :custom ((visual-fill-column-width 110)
                       (visual-fill-column-center-text t)))

(setq org-roam-v2-ack t)
(use-package org-roam
  :after org
  :init
  (setq ytg/org-roam-dir (if ytg/roam-use-home "~/Documents/OrgRoam/"
                           (if (getenv "XDG_DATA_DIR")
                               (expand-file-name "emacs/org-roam/" (getenv "XDG_DATA_DIR"))
                             "~/.local/share/emacs/org-roam/")))
  (setq org-roam-v2-ack t)
  (mkdir (expand-file-name "daily/" ytg/org-roam-dir) t)
  :custom ((org-roam-directory ytg/org-roam-dir)
           (org-roam-completion-everywhere t)
           (org-roam-dailies-capture-templates
            '(("d" "default" entry "* %?"
               :target (file+head "%<%Y-%m-%d>.org"
                                  "#+title: %<%Y-%m-%d>\n"))
              ("t" "timed" entry "* %<%H:%M %p>: %?"
               :target (file+head "%<%Y-%m-%d>.org" "#+title: %<%Y-%m-%d>\n"))))
           (org-roam-capture-templates
            '(("d" "default" plain "%?"
               :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
               :unnarowed t)
              ("l" "language" plain
               "%?\n\n* Info:\n- Family: \n- Branch: \n- Name: ${title} (%^{Name in-language})\n"
               :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                                  "#+title: ${title}\n#+filetags: Language\n")
               :unnarowed t)
              ("a" "computer application" plain
               "%?\n\n* Info:\n- Author(s): \n- Programming Language: \n- Year: %^{Year}\n- Name: ${title}\n- License: %^{License}"
               :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                                  "#+title: ${title}\n#+filetags: Computer Program\n")
               :unnarowed t)
              ("b" "book notes" plain
               "* Source\n- Author: \n- Title: ${title}\n- Year: %^{Year}\n\n* Summary\n%?"
               :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                                  "#+title: ${title}\n#+filetags: Book Notes\n")
               :unnarowed t)
              ("p" "project" plain
               "* Information\nName: ${title}\nType: %^{Project Type||Software Project|School Project}\n\n** Goal\n%?\n\n* Tasks\n** TODO Add tasks\n\n* Dates\n"
               :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                                  "#+title: ${title}\n#+filetags: Project\n")
               :unnarowed t)
              ("t" "todo" plain
               "* TODO ${title}\n%?"
               :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
               :unnarowed t)
              ("h" "person" plain
               "* General Information\n- Born %^{Born}\n- Native Full Name: %^{Full Name (N)}\n- English Full Name: %^{Full Name (en)}\n- Langlish Full Name: %^{Full Name (Lang)}\n- Personally Know: %^{Personally Know?}\n\n* %?\n"
               :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
               :unnarowed t))))
  :general
  ("C-c n l" 'org-roam-buffer-toggle
   "C-c n f" '(org-roam-node-find :which-key "Find Node")
   "C-c n i" '(org-roam-node-insert :which-key "Insert Node")
   "C-c n d" '(:keymap org-roam-dailies-map :which-key "Dailies"))
  (:keymaps 'org-mode-map
        "C-M-i" 'completion-at-point)
  (:keymaps 'org-roam-dailies-map
        "Y" 'org-roam-dailies-capture-yesterday
        "T" 'org-roam-dailies-capture-tomorrow)
  :config
  (require 'org-roam-dailies)
  (org-roam-db-autosync-mode 1)
  (setq org-roam-v2-ack t))

(use-package hebrew-2-input ;; In the local load-path
  :straight nil
  :defer t)
(require 'hebrew-2-input)

(use-package restclient
  :mode "\\.http\\'")

(use-package company-restclient
  :after (company restclient)
  :config (add-to-list 'company-backends 'company-restclient)
  :hook (restclient-mode . company-mode))

(use-package ob-restclient
  :after org
  :config (org-babel-do-load-languages
           'org-babel-load-languages
           '((restclient . t))))

(use-package vterm :defer t)

(use-package elfeed
  :commands (elfeed)
  ;; :after evil
  :config
  (setq-default elfeed-search-filter "@2-days-ago +unread"))

(use-package elfeed-org
  :after elfeed
  :config
  (require 'elfeed-org)
  (elfeed-org)
  (setq rmh-elfeed-org-files
        (let ((elfeed-org-dir
               (expand-file-name "elfeed.d/" no-littering-etc-directory)))
          (make-directory elfeed-org-dir t)
          (directory-files elfeed-org-dir t "\.org$" t))))

(defun %ytg/mpd-callback (_ resp) (message resp))
(defun %ytg/emms-mpd-setup ()
  (dolist (cmd ytg/mpd-options)
    (emms-player-mpd-send cmd nil #'%ytg/mpd-callback))
  (emms-player-mpd-send "status" nil #'%ytg/mpd-callback))

(use-package emms
  :defer t
  :commands (emms emms-playlist-mode-go emms-smart-browse emms-browser)
  :config
  (require 'emms-setup)
  (require 'emms-player-mpd)
  (require 'emms-lyrics)
  (emms-all)
  (emms-lyrics 1)
  (setq emms-player-list '(emms-player-mpd))
  (add-to-list 'emms-info-functions 'emms-info-mpd)
  (emms-player-mpd-connect) ;; Connect to MPD when starting
  (%ytg/emms-mpd-setup)
  :custom ((emms-player-mpd-server-name "localhost")
           (emms-player-mpd-server-port "6600")
           (emms-player-mpd-music-directory "~/Music")
           (emms-volume-change-function 'emms-volume-mpd-change)
           (emms-lyrics-scroll-p nil)))

(defun ytg/emms-mpd-reconnect ()
  (interactive)
  (require 'emms-player-mpd)
  (emms-player-mpd-disconnect)
  (emms-player-mpd-connect)
  (%ytg/emms-mpd-setup)
  (emms-player-mpd-update-all-reset-cache))

(use-package pdf-tools
  :config (pdf-loader-install))

(use-package yasnippet
  :hook (after-init . yas-global-mode)
  :diminish (yas-minor-mode . "ys")
  :config
  (require 'yasnippet))

(use-package yasnippet-snippets
  :after yasnippet)

(use-package company
  :commands (company-mode)
  :diminish company-mode
  ;; :after lsp-mode
  :hook (prog-mode . company-mode)
  :general
  (:keymaps 'company-active-map
            "<tab>" 'company-abort
            "C-M-j" 'company-abort)
  ;; (:keymaps 'lsp-mode-map
  ;;           "<tab>" 'indent-for-tab-command)
  :custom ((company-minimum-prefix-length 1)
           (company-idle-delay 0.0))
  :config (dolist (mode '(sh-mode-hook))
            (add-hook mode (lambda () (company-mode -1)))))

(when %ytg/graphics
  (use-package company-box
    :after company
    :diminish company-box-mode
    :hook (company-mode . company-box-mode)))

(use-package dash)

(use-package treemacs
  :commands treemacs)

;; (use-package treemacs-evil ; Keybinds!
;;   :after treemacs)

;; (use-package lsp-treemacs ; Show code symbols in tree
;;   :after (lsp treemacs))

;; (use-package treemacs-projectile ; Integration with projectile
;;   :after (projectile treemacs))

(use-package treemacs-all-the-icons ; Theme using all-the-icons
  :after (all-the-icons treemacs)
  :config (treemacs-load-theme "all-the-icons"))

(use-package dap-mode
  :commands (dap-mode dap-debug)
  :diminish dap-mode)

;; Mostly from Doom Emacs
(use-package flycheck
  :hook (after-init . global-flycheck-mode)
  :diminish (flycheck-mode . "fc")
  :custom ((flycheck-emacs-lisp-load-path 'inherit)
           ;; Don't recheck on idle as often
           (flycheck-idle-change-delay 1.0)
           ;; Check syntax in a buffer that you switched to
           ;; only briefly. This allows "refreshing" the syntax check state for several
           ;; buffers quickly after e.g. changing a config file.
           (flycheck-buffer-switch-check-intermediate-buffers t)
           ;; Display errors quicker
           (flycheck-display-errors-delay 0.25))
  ;; Rerunning checks on every newline is excessive
  :config (delq 'new-line flycheck-check-syntax-automatically))

(use-package langtool
  :commands (langtool-check-buffer langtool-check-done langtool-correct-buffer langtool-show-message-at-point)
  :config
  (setq langtool-language-tool-jar ytg/languagetool-cmdline-path)
  (require 'langtool)
  (general-define-key ;; ytg/global-key
    "C-x c c" 'langtool-check-buffer
    "C-x c p" 'langtool-show-message-at-point
    "C-x c s" 'langtool-correct-buffer
    "C-x c x" 'langtool-check-done))

(setq %ytg/sourcekit
      (cond
       ((equal system-type 'darwin) '("/usr/bin/xcrun" "sourcekit-lsp"))
       ((equal system-type 'gnu/linux) (list (executable-find "sourcekit-lsp")))))

(use-package eglot
  :after (typescript-mode
          swift-mode
                rust-mode
          rustic)
  :custom (rustic-lsp-client 'eglot)
  :config (dolist (server `((swift-mode . ,%ytg/sourcekit)))
            (add-to-list 'eglot-server-programs server))
  :hook ((typescript-mode       . eglot-ensure)
         (js-mode               . eglot-ensure)
         (python-mode           . eglot-ensure)
         (swift-mode            . eglot-ensure)
         (c-mode-common         . eglot-ensure)
         (tex-mode              . eglot-ensure)
         (latex-mode            . eglot-ensure)
         (LaTeX-mode            . eglot-ensure)
         (html-mode             . eglot-ensure)
         (cmake-mode            . eglot-ensure)
         (go-mode               . eglot-ensure)
         (rustic-mode           . eglot-ensure)
               (rust-mode             . eglot-ensure)
         (haskell-mode          . eglot-ensure)
         (haskell-literate-mode . eglot-ensure)
         (zig-mode              . eglot-ensure)))

(when (featurep 'treesit)
  (setq ytg/treesit-dir (expand-file-name "treesit" ytg/emacs-data))
  (add-to-list 'treesit-extra-load-path ytg/treesit-dir)
  (setq
   treesit-language-source-alist
   '((bash "https://github.com/tree-sitter/tree-sitter-bash")
       (css "https://github.com/tree-sitter/tree-sitter-css")
       (elisp "https://github.com/Wilfred/tree-sitter-elisp")
       (go "https://github.com/tree-sitter/tree-sitter-go")
       (html "https://github.com/tree-sitter/tree-sitter-html")
       (javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
       (json "https://github.com/tree-sitter/tree-sitter-json")
       (make "https://github.com/alemuller/tree-sitter-make")
       (markdown "https://github.com/ikatyang/tree-sitter-markdown")
       (python "https://github.com/tree-sitter/tree-sitter-python")
       (toml "https://github.com/tree-sitter/tree-sitter-toml")
       (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")
       (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
       (yaml "https://github.com/ikatyang/tree-sitter-yaml")
       (rust "https://github.com/tree-sitter/tree-sitter-rust" "master" "src")
       (scheme "https://github.com/6cdh/tree-sitter-scheme" "main" "src")
       (common-lisp "https://github.com/theHamsta/tree-sitter-commonlisp" "master" "src")
       (php "https://github.com/tree-sitter/tree-sitter-php" "master" "php/src")
       (haskell "https://github.com/tree-sitter/tree-sitter-haskell" "master" "src")
       (latex "https://github.com/latex-lsp/tree-sitter-latex" "master" "src")
       (zig "https://github.com/maxxnino/tree-sitter-zig" "main" "src")
     (c "https://github.com/tree-sitter/tree-sitter-c" "master" "src")
       (cpp "https://github.com/tree-sitter/tree-sitter-cpp" "master" "src")
       (ocaml "https://github.com/tree-sitter/tree-sitter-ocaml" "master" "ocaml/src")
       (ocaml-interface "https://github.com/tree-sitter/tree-sitter-ocaml" "master" "interface/src")
       (nix "https://github.com/nix-community/tree-sitter-nix" "master" "src")))
  (setq treesit-load-name-override-list '((common-lisp "libtree-sitter-common-lisp" "tree_sitter_commonlisp")))
  (mapc
   (lambda (lang)
       (unless (treesit-language-available-p lang) (treesit-install-language-grammar lang ytg/treesit-dir)))
   (mapcar #'car treesit-language-source-alist))
  (setq
   major-mode-remap-alist
   '((yaml-mode . yaml-ts-mode)
       (js2-mode . js-ts-mode)
       (typescript-mode . typescript-ts-mode)
       (css-mode . css-ts-mode)
       (python-mode . python-ts-mode)
       (go-mode . go-ts-mode)
       (toml-mode . toml-ts-mode)
       (c-mode . c-ts-mode)
       (c++-mode . c++-ts-mode))))

(use-package typescript-mode
  :mode "\\.ts\\'"
  :hook ((typescript-mode .
                          (lambda ()
                            (setq tab-width ytg/js-indent-level))))
  :custom (typescript-indent-level ytg/js-indent-level))

(use-package swift-mode
  :mode "\\.swift\\'")

(use-package js2-mode
:mode "\\.js\\'"
:hook ((js-mode . js2-minor-mode)
       (js-mode . (lambda ()
                    (setq tab-width ytg/js-indent-level))))
:custom (js-indent-level ytg/js-indent-level)
:config (add-to-list 'interpreter-mode-alist '("node" . js2-mode)))

(use-package flymake-shellcheck
  :commands flymake-shellcheck-load
  :hook ((sh-mode . flymake-shellcheck-load)
         (sh-mode . flymake-mode)))

(use-package cmake-mode
  :straight nil)

(defun ytg/markdown-setup ()
  (variable-pitch-mode 1) ; non-monospace font
  (visual-line-mode 1))

(defun ytg/md-font-setup ()
  ;; Bulleted Lists
  (font-lock-add-keywords 'markdown-mode '(("^ *\\([-*]\\) "
                                            (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  ;; Title Sizes
  (dolist (face '((markdown-header-face-1 . 1.4)
                  (markdown-header-face-2 . 1.2)
                  (markdown-header-face-3 . 1.1)
                  (markdown-header-face-4 . 1.05)
                  (markdown-header-face-5 . 1.0)
                  (markdown-header-face-6 . 1.1)))
    (set-face-attribute (car face) nil :weight 'regular :height (cdr face)))

  ;; Monospace for things that need it
  (set-face-attribute 'markdown-table-face nil :inherit 'fixed-pitch)

  (set-face-attribute 'markdown-html-entity-face nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'markdown-html-tag-name-face nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'markdown-html-tag-delimiter-face nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'markdown-html-attr-name-face nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'markdown-html-attr-value-face nil :inherit '(shadow fixed-pitch))

  (set-face-attribute 'markdown-code-face nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'markdown-table-face nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'markdown-inline-code-face nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'markdown-metadata-key-face nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'line-number nil :inherit 'fixed-pitch)
  (set-face-attribute 'line-number-current-line nil :inherit 'fixed-pitch))

(use-package markdown-mode
  :hook (markdown-mode . ytg/markdown-setup)
  :mode "\\.md\\'"
  :config (ytg/md-font-setup)
  :custom ((markdown-fontify-code-blocks-natively t)))

(use-package yaml-mode
  :mode "\\.ya?ml\\'")

(use-package go-mode
  :mode "\\.go\\'")

(use-package rust-mode
  :custom ((rust-mode-treesitter-derive t)
                 (rust-format-on-save t))
  :hook ((rust-mode . (lambda ()
                                         (setq indent-tabs-mode nil)))
               (rust-mode . prettify-symbols-mode)))

(use-package flycheck-rust
  :after rust-mode
  :hook (flycheck-mode . flycheck-rust-setup))

(use-package systemd-mode
  :mode "\\.((service)|(timer)|(network))\\'"
  :straight (systemd-mode :type git
                           :host github
                           :repo "holomorph/systemd-mode"
                           :files (:defaults
                                   "unit-directives.txt"
                                   "network-directives.txt"
                                   "nspawn-directives.txt")))

(use-package geiser
  :defer t)
(use-package geiser-guile
  :after geiser)

(use-package sly
  :hook ((lisp-mode-local-vars . sly-editing-mode))
         ;; (sly-mode . evil-normalize-keymaps))
  :custom ((sly-kill-without-query-t t)
           (sly-net-coding-system 'utf-8-unix)
           (sly-complete-symbol-function 'sly-flex-completions))
  :config (setq sly-mrepl-history-file-name
                (expand-file-name "sly/mrepl-history"
                                  (or (getenv "XDG_STATE_HOME")
                                      (expand-file-name ".local/state/" (getenv "HOME"))))))

(use-package sly-macrostep
  :after sly)

(use-package sly-repl-ansi-color
  :after sly
  :init (add-to-list 'sly-contribs 'sly-repl-ansi-color))

(eval
 `(use-package gdscript-mode ; Install gdtoolkit from PyPi for formatting
      :after hydra
      :straight ,(if %ytg/home-manager nil
                               '(gdscript-mode :type git
                                                               :host github
                                                               :repo "godotengine/emacs-gdscript-mode"))
      :custom ((gdscript-use-tab-indents t)
                       (gdscript-indent-offset 4)
                       (gd-script-gdformat-save-and-format t))))

(use-package gnuplot-mode
  :commands (gnuplot-mode)
  :mode "\\.(gnuplot|gp)\\'")

(use-package php-mode
  :defer t)

(use-package web-mode
  :mode ("\\.phtml\\'"
         "\\.php\\'"
         "\\.html?\\'"))

(use-package haskell-mode
  :defer t
  :mode ("\\.hs\\'"))

(use-package ocaml-ts-mode
  :commands (ocaml-ts-mode)
  :mode "\\.ml\\'"
  :hook (ocaml-ts-mode . (lambda () (whitespace-mode -1))))
(use-package merlin
  :after (ocaml-ts-mode)
  :hook (ocaml-ts-mode . merlin-mode))
(use-package merlin-company
  :after (merlin company)
  :config (add-to-list 'company-backends 'merlin-company-backend)
  :hook (merlin-mode . company-mode))
(use-package merlin-eldoc
  :after (merlin)
  :hook (merlin-mode . merlin-eldoc-setup))

(eval
 `(use-package tex
      :straight ,(if %ytg/home-manager nil 'auctex)
      :defer t
      :config
      (require 'tex)
      (require 'tex-site)
      :custom ((TeX-auto-save t)
                       (TeX-parse-self t)
                       (TeX-fold-auto t)
                       (TeX-fold-preserve-comments t)
                       (prettify-symbols-unprettify-at-point t)
                       (TeX-engine 'luatex))
      :hook
      ((LaTeX-mode . company-mode)
       (LaTeX-mode . (lambda () (TeX-fold-mode 1)))
       ;; (LaTeX-mode . prettify-symbols-mode)
       )))

(use-package lilypond-mode
  :if (locate-library "lilypond-mode")
  :mode "\\.\\(ly\\|ily\\)\\'"
  :straight nil)

(use-package highlight-defined
  :custom (highlight-defined-face-use-itself t)
  :hook ((help-mode . highlight-defined-mode)
         (emacs-lisp-mode . highlight-defined-mode)))

(use-package highlight-quoted
  :hook (emacs-lisp-mode . highlight-quoted-mode))

(use-package highlight-sexp
  :straight (highlight-sexp :type git :host github
                            :repo "daimrod/highlight-sexp")
  :diminish highlight-sexp-mode
  :hook (lisp-data-mode . highlight-sexp-mode))

(use-package eros
  :hook (emacs-lisp-mode . eros-mode))

(use-package suggest
  :defer t
  :commands (suggest suggest-update))

(use-package elsa
  :defer t)

(use-package flycheck-elsa
  :after (flycheck elsa)
  :hook (emacs-lisp-mode . flycheck-elsa-setup))

(use-package paredit
  :commands (enable-paredit-mode)
  :diminish paredit-mode
  :config
  ;; Paredit breaks evaluation because it rebinds RET
  (defvar %ytg/paredit-map-RET (lookup-key paredit-mode-map (kbd "RET")))
  (defun %ytg/paredit-minibuf-setup ()
    (enable-paredit-mode)
    (unbind-key (kbd "RET") paredit-mode-map))
  (defun %ytg/paredit-minibuf-exit ()
    (bind-key (kbd "RET") %ytg/paredit-map-RET paredit-mode-map))
  :hook ((emacs-lisp-mode . enable-paredit-mode)
         (eval-expression-minibuffer-setup . %ytg/paredit-minibuf-setup)
         (minibuffer-exit . %ytg/paredit-minibuf-exit)
         (ielm-mode . enable-paredit-mode)
         (lisp-mode . enable-paredit-mode)
         (lisp-interaction-mode . enable-paredit-mode)
         (scheme-mode . enable-paredit-mode)))

(use-package eldoc
  :straight nil
  :after paredit
  :config (eldoc-add-command
           'paredit-backward-delete
           'paredit-close-round))

(use-package zig-mode
  :mode "\\.zig\\'")

(use-package nix-mode
  :mode "\\.nix\\'")

(use-package dap-mode
;; :after lsp-mode
:custom ((dap-python-executable "python3")
         (dap-python-debugger 'debugpy))
:config
(require 'dap-python))

(use-package dap-mode
  :config (require 'dap-lldb))

(use-package ripgrep
  :commands (ripgrep-regexp))

(use-package emmet-mode
  :hook ((html-mode . emmet-mode)
         (css-mode . emmet-mode)
         (sgml-mode . emmet-mode))
  :functions (emmet-mode)
  :custom (emmet-self-closing-tag-style " /"))

(use-package skewer-mode
  :hook ((js2-mode . skewer-mode)
         (css-mode . skewer-mode)
         (html-mode . skewer-mode))
  :commands (skewer-mode))

(use-package hyperbole
  :diminish hyperbole-mode
  :defer t
  :config
  (setq hbmap:dir-user (expand-file-name "hyperbole/"
                                         (or (getenv "XDG_DATA_HOME")
                                             (expand-file-name ".local/share/" (getenv "HOME")))))
  (hyperbole-mode 1))

(url-handler-mode 1)

(defun doom--sudo-file-path (file)
  (let ((host (or (file-remote-p file 'host) "localhost")))
    (concat "/" (when (file-remote-p file)
                  (concat (file-remote-p file 'method) ":"
                          (if-let (user (file-remote-p file 'user))
                              (concat user "@" host)
                            host)
                          "|"))
            "doas:root@" host
            ":" (or (file-remote-p file 'localname)
                    file))))

(defun doom/sudo-find-file (file)
  "Open FILE as root."
  (interactive "FOpen file as root: ")
  (find-file (doom--sudo-file-path file)))

(defun doom/sudo-this-file ()
  "Open the current file as root."
  (interactive)
  (find-file
   (doom--sudo-file-path
    (or buffer-file-name
        (when (or (derived-mode-p 'dired-mode)
                  (derived-mode-p 'wdired-mode))
          default-directory)))))

(defun doom/sudo-save-buffer ()
  "Save this file as root."
  (interactive)
  (let ((file (doom--sudo-file-path buffer-file-name)))
    (if-let (buffer (find-file-noselect file))
        (let ((origin (current-buffer)))
          (copy-to-buffer buffer (point-min) (point-max))
          (unwind-protect
              (with-current-buffer buffer
                (save-buffer))
            (unless (eq origin buffer)
              (kill-buffer buffer))
            (with-current-buffer origin
              (revert-buffer t t))))
      (user-error "Unable to open %S" file))))

(cl-case system-type
  ((darwin)
   (setq ns-right-alternate-modifier 'none)
   (setq ns-alternate-modifier 'meta)))

(use-package exec-path-from-shell
  :if (or (daemonp) (memq window-system '(mac ns)))
  :custom
  (exec-path-from-shell-arguments . nil)
  :init
  (require 'exec-path-from-shell)
  (setq exec-path-from-shell-variables '("PATH" "MANPATH" "LD_LIBRARY_PATH"
                                         "SSH_AUTH_SOCK" "SSH_AGENT_PID"
                                         "GPG_AGENT_INFO" "LANG" "LC_CTYPE"
                                         "NIX_SSL_CERT_FILE" "NIX_PATH" "INFOPATH"
                                         "XDG_CONFIG_HOME" "XDG_DATA_HOME" "XDG_CACHE_HOME" "XDG_STATE_HOME"))
  (exec-path-from-shell-initialize))
