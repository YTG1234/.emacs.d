;;; hebrew-2-input -- Hebrew SI-1452-2 input method for Emacs.

;;; Commentary:
;; I manually entered every char.

;;; Code:

(quail-define-package
 "hebrew-si-1452-2" "Hebrew" "ע" t
 "Hebrew SI-1452-2 input method.

Based on SI-1452-2 keyboard layout.
Only characters that differ from US QWERTY are considered.
 \'/\' is used to switch levels instead of Alt-Gr.
 Maqaaf (־) is mapped to \'/-\'.

KEYBOARD LAYOUT
---------------
This input method works by translating individual input characters.
Assuming that your actual keyboard has the 'standard' layout,
translation results in a similar keyboard layout to the 'hebrew' method,
with the following changes:

  All punctuation marks, except for \';\', are like English.
  ת moves to the space previously occupied by ן.
  ן moves to the space previously occupied by \'.
  ץ moves to the space previously occupied by /.
  Niqqud is written like Windows (\'/\' for the third level).

Invisible characters:
  \'/9\' is the left-to-right mark.
  \'/0\' is the right-to-left mark.
  \'/(\' is the left-to-right isolate (extension).
  \'/)\' is the right-to-left isolate (extension).
  \'/_\' is pop directional isolate (extension).
  \'/+\' is first strong isolate.

There are also additional symbols, see C-h I hebrew-si-1452-2 RET."

 nil t nil nil nil nil nil nil nil nil t)

(quail-define-rules
 ("`" ";")
 ("/`" "׳")
 ("/3" "€")
 ("/4" "₪")
 ("/5" "°")
 ("/6" "֫")
 ("/7" "ֽ")
 ("/8" "×")
 ("/9" "‎")
 ("/0" "‏")
 ("/-" "־")
 ("/=" "–")
 ("(" ")")
 (")" "(")
 ("/(" "⁦")
 ("/)" "⁧")
 ("/_" "⁩")
 ("/+" "⁨")

 ("q" "ץ")
 ("/q" "ׂ")
 ("w" "ן")
 ("/w" "ׁ")
 ("e" "ק")
 ("/e" "ָ")
 ("r" "ר")
 ("/r" "ֳ")
 ("t" "א")
 ("y" "ט")
 ("u" "ו")
 ("/u" "ֹ")
 ("i" "ת")
 ("/i" "ׇ")
 ("o" "ם")
 ("p" "פ")
 ("[" "]")
 ("]" "[")
 ("{" "}")
 ("}" "{")
 ("/p" "ַ")
 ("/[" "ֲ")
 ("/]" "ֿ")
 ("/\\" "ֻ")

 ("a" "ש")
 ("/a" "ְ")
 ("s" "ד")
 ("/s" "ּ")
 ("d" "ג")
 ("f" "כ")
 ("g" "ע")
 ("h" "י")
 ("j" "ח")
 ("/j" "ִ")
 ("k" "ל")
 ("l" "ך")
 ("/l" "”")
 (";" "ף")
 ("/;" "„")
 ("'" "'")
 ("\"" "\"")
 ("//`" "`")
 ("/'" "״")

 ("z" "ז")
 ("x" "ס")
 ("/x" "ֶ")
 ("c" "ב")
 ("/c" "ֱ")
 ("v" "ה")
 ("b" "נ")
 ("n" "מ")
 ("m" "צ")
 ("/m" "ֵ")
 ("<" ">")
 (">" "<")
 ("/," "’")
 ("/." "‚")
 ("//" "÷")
 ("///" "/"))

(provide 'hebrew-2-input)
;;; hebrew-2-input.el ends here
